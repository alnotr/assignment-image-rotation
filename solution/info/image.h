#ifndef IMAGE_TRANSFORMER_IMAGE_H
#define IMAGE_TRANSFORMER_IMAGE_H

#include <inttypes.h>

#pragma pack(push, 1)
struct image {
    uint64_t width, height;
    struct pixel *data;
};
#pragma pack(pop)

struct pixel {
    uint8_t b, g, r;
};

#endif //IMAGE_TRANSFORMER_IMAGE_H
