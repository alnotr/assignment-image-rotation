#ifndef IMAGE_TRANSFORMER_STATUS_H
#define IMAGE_TRANSFORMER_STATUS_H

enum read_status {
    READ_OK = 0,
    READ_INVALID_SIGNATURE,
    READ_INVALID_BITS,
    READ_INVALID_HEADER,
    READ_ERROR
};

enum write_status {
    WRITE_OK = 0,
    WRITE_ERROR
};

enum rotate_status{
    ROTATE_OK = 0,
    ROTATE_ERROR
};

enum file_open_status{
    OPEN_OK = 0,
    OPEN_ERROR
};

#endif //IMAGE_TRANSFORMER_STATUS_H
