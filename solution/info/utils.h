#ifndef IMAGE_TRANSFORMER_UTILS_H
#define IMAGE_TRANSFORMER_UTILS_H

#include "bmp.h"
#include <inttypes.h>
#include <stdio.h>
#include <stdlib.h>


enum read_status from_bmp(FILE *in, struct image *img);

enum write_status to_bmp(FILE *out, struct image *img);

enum rotate_status rotate(struct image *img);

enum file_open_status in_file_open(FILE **in, char *in_file_name);

enum file_open_status out_file_open(FILE **out, char *out_file_name);

void image_free(struct image *img);



int8_t getPadding(struct image *img);

#endif //IMAGE_TRANSFORMER_UTILS_H
