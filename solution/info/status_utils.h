
#ifndef IMAGE_TRANSFORMER_ERROR_UTILS_H

#include "status.h"
#include <stdio.h>

#define IMAGE_TRANSFORMER_ERROR_UTILS_H

void write_read_status(enum read_status status, FILE* stream);

void write_write_status(enum write_status status, FILE* stream);

void write_rotate_status(enum rotate_status status, FILE* stream);

void write_open_file_status(enum file_open_status status, FILE* stream);

#endif //IMAGE_TRANSFORMER_ERROR_UTILS_H
