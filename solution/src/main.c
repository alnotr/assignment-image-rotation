#include "./../info/image.h"
#include "./../info/status_utils.h"
#include "./../info/utils.h"

#define SUCCESS 0

int main(int argc, char **argv) {
    if (argc < 3) {
        fprintf(stderr, "Not enough arguments");
    }
    FILE *in_file = NULL;
    FILE *out_file = NULL;
    enum file_open_status fileOpenStatus = in_file_open(&in_file, argv[1]);
    write_open_file_status(fileOpenStatus, fileOpenStatus == 0 ? stdout : stderr);
    fileOpenStatus = out_file_open(&out_file, argv[2]);
    write_open_file_status(fileOpenStatus, fileOpenStatus == 0 ? stdout : stderr);
    struct image img;
    enum read_status readStatus = from_bmp(in_file, &img);
    write_read_status(readStatus, readStatus == 0 ? stdout : stderr);
    struct pixel* data = img.data;
    enum rotate_status rotateStatus = rotate(&img);
    write_rotate_status(rotateStatus, rotateStatus == 0 ? stdout : stderr);
    enum write_status writeStatus = to_bmp(out_file, &img);
    write_write_status(writeStatus, writeStatus == 0 ? stdout : stderr);
    image_free(&img);
    free(data);
    fclose(in_file);
    fclose(out_file);
    return SUCCESS;
}
