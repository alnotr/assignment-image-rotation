#include "./../../info/status_utils.h"

void write_read_status(enum read_status status, FILE* stream) {
    char* status_array[5] = {"READ OK\n","READ ERROR\n", "READ INVALID HEADER\n",
                           "READ INVALID BITS\n", "READ INVALID SIGNATURE\n"};
    fprintf(stream, "%s", status_array[status]);
}

void write_write_status(enum write_status status, FILE* stream) {
    char* status_array[2] = {"WRITE OK\n", "WRITE ERROR\n"};
    fprintf(stream, "%s", status_array[status]);
}

void write_rotate_status(enum rotate_status status, FILE* stream) {
    char* status_array[2] = {"ROTATE OK\n", "ROTATE ERROR\n"};
    fprintf(stream, "%s", status_array[status]);
}

void write_open_file_status(enum file_open_status status, FILE* stream){
    char* status_array[2] = {"OPEN FILE OK\n", "OPEN FILE ERROR\n"};
    fprintf(stream, "%s", status_array[status]);
}
