#include "stdlib.h"
#include "./../../info/utils.h"


static enum read_status read_data(FILE *in, struct image *img) {
    int8_t padding = getPadding(img);
    struct pixel *data = malloc(sizeof(struct pixel) * img->height * img->width);
    for (size_t i = 0; i < img->height; i++) {
        uint64_t count_of_read_bytes = fread(data + img->width * i, sizeof(struct pixel), img->width, in);
        if (count_of_read_bytes != img->width) {
            free(data);
            return READ_ERROR;
        }
        if(fseek(in,padding,SEEK_CUR) != 0) {
            free(data);
            return READ_ERROR;
        }
    }
    img->data = data;
    return READ_OK;
}

struct image* create_image(struct bmp_header header, struct image *img){
    img->width = header.biWidth;
    img->height = header.biHeight;
    return img;
}

enum read_status from_bmp(FILE *in, struct image *img) {
    struct bmp_header header;
    if (!fread(&header, sizeof(struct bmp_header), 1, in)) {
        return READ_INVALID_HEADER;
    }
    img = create_image(header,img);
    if (read_data(in, img)) {
        free(img->data);
        return READ_ERROR;
    }
    return READ_OK;
}
