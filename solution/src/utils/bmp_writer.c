#include "./../../info/utils.h"
#include "./../../info/bmp.h"



static enum write_status write_header(FILE *out, struct image *img) {
    struct bmp_header out_header = createBmpHeaderFromImage(img);
    if (!fwrite(&out_header, sizeof(struct bmp_header), 1, out)) {
        return WRITE_ERROR;
    }
    return WRITE_OK;
}

static enum write_status write_data(FILE *out, struct image *img) {
    const int8_t padding = getPadding(img);
    const uint64_t white_space[3] = {0};
    for (int i = 0; i < img->height; i++) {
        if (fwrite(img->data + img->width * i, sizeof(struct pixel), img->width, out) != img->width) {
            return WRITE_ERROR;
        }
        if (!fwrite(white_space, padding, 1, out) && padding != 0) {
            return WRITE_ERROR;
        }
    }
    return WRITE_OK;
}

enum write_status to_bmp(FILE *out, struct image *img) {
    if (write_header(out, img)) {
        return WRITE_ERROR;
    }
    if (write_data(out, img)) {
        return WRITE_ERROR;
    }
    return WRITE_OK;
}
