#include "./../../info/utils.h"

enum rotate_status rotate(struct image *img) {
    struct pixel *new_pixels = malloc(img->width * img->height * sizeof(struct pixel));
    for (uint64_t i = 0; i < (img->height); i++) {
        for (uint64_t j = 0; j < (img->width); j++) {
            new_pixels[(j * img->height) + i] = img->data[((img->height - 1 - i) * img->width) + j];
        }
    }
    uint64_t height = img->height;
    img->height = img->width;
    img->width = height;
    img->data = new_pixels;
    return ROTATE_OK;
}

void image_free(struct image *img) {
    free(img->data);
}
