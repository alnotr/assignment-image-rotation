#include "./../../info/utils.h"

#define IN_FILE_MODE "rb"
#define OUT_FILE_MODE "wb"

enum file_open_status in_file_open(FILE **in, char *in_file_name) {
    *in = fopen(in_file_name, IN_FILE_MODE);
    if(in == NULL){
        return OPEN_ERROR;
    }
    return OPEN_OK;
}

enum file_open_status out_file_open(FILE **out, char *out_file_name) {
    *out = fopen(out_file_name, OUT_FILE_MODE);
    if(out == NULL){
        return OPEN_ERROR;
    }
    return OPEN_OK;
}
