//
// Created by Алёна on 2i2.01.2023.
//
#include "./../../info/bmp.h"
#include "./../../info/utils.h"

int8_t getPadding(struct image *img) {
    return (int8_t)((4 - (img->width * sizeof(struct pixel) % 4)) % 4);
}

enum standard_bmpHeader_values {
    standard_bfType = 19778,
    standard_bfReserved = 0,
    standard_bOffBits = sizeof(struct bmp_header),
    standard_biPlanes = 1,
    standard_biCompression = 0,
    standard_biXPelsPerMeter = 2834,
    standard_biYPelsPerMeter = 2834,
    standard_biClrUsed = 0,
    standard_biClrImportant = 0,
    standard_biBitCount = 24,
    standard_biSize = 40
};

static struct bmp_header createStandardBmpHeader(void) {
    return (struct bmp_header) {.bfType=standard_bfType, .bfReserved=standard_bfReserved, .bOffBits=standard_bOffBits,
            .biPlanes=standard_biPlanes, .biCompression=standard_biCompression,
            .biXPelsPerMeter=standard_biXPelsPerMeter, .biYPelsPerMeter=standard_biYPelsPerMeter,
            .biClrUsed=standard_biClrUsed, .biClrImportant=standard_biClrImportant,
            .biBitCount=standard_biBitCount, .biSize=standard_biSize};
}

struct bmp_header createBmpHeaderFromImage(struct image *img) {
    struct bmp_header out_header = createStandardBmpHeader();
    size_t size = img->height * img->width * sizeof(struct pixel);
    out_header.biHeight = img->height;
    out_header.biWidth = img->width;
    out_header.biSizeImage = size;
    out_header.bfileSize = sizeof(struct bmp_header) + size;
    return out_header;
}
